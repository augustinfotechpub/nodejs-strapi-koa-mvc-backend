'use strict';

const twilio = {
    id: process.env.Account_SID,
    token: process.env.Auth_Token,
    phone: process.env.Twilio_phone_number
  }
  console.log(twilio.id, twilio.token , "ID,token");
  const smsClient = require('twilio')(twilio.id, twilio.token);
  module.exports = {
    // async create(ctx){
    //     const { mobileNumber, username } = ctx.request.body;
    //     console.log(ctx.request.body , "data");
    //     if (!mobileNumber) return ctx.badRequest('missing.mobileNumber');
    //     if (!username) return ctx.badRequest('missing.username');
    
    
    //     const userWithThisNumber = await strapi
    //       .query('user', 'users-permissions')
    //       .findOne({ mobileNumber });
    
    //     if (userWithThisNumber) {
    //       return ctx.badRequest(
    //         null,
    //         formatError({
    //           id: 'Auth.form.error.phone.taken',
    //           message: 'Phone already taken.',
    //           field: ['mobileNumber'],
    //         })
    //       );
    //     }
    
    //     const token = Math.floor(Math.random() * 90000) + 10000;
        
    //     const user = {
    //             username,
    //             mobileNumber,
    //       provider: 'local',
    //       token
    //     };
    
    //     const advanced = await strapi
    //       .store({
    //         environment: '',
    //         type: 'plugin',
    //         name: 'users-permissions',
    //         key: 'advanced',
    //       })
    //       .get();
    
    //     const defaultRole = await strapi
    //       .query('role', 'users-permissions')
    //       .findOne({ type: advanced.default_role }, []);
    
    //     user.role = defaultRole.id;
    
    
    //     try {
    //       const data = await strapi.plugins['users-permissions'].services.user.add(user);
    //       await smsClient.messages.create({
    //         to:  mobileNumber,
    //         from: twilio.phone,
    //         body: `Your verification code is ${token}`
    //       })
    //       ctx.created(sanitizeUser(data));
    //     } catch (error) {
    //       ctx.badRequest(null, formatError(error));
    //     }
    // }, 

    async verifyAccount(ctx) {

        const { mobileNumber, token } = ctx.request.body;
    
        if (!mobileNumber) return ctx.badRequest('missing.phone');
  
    
        const verifyUserCode = await strapi
          .query('user', 'users-permissions')
          .findOne({ mobileNumber });
    
        if (!verifyUserCode) {
          return ctx.badRequest(
            null,
            formatError({
              id: 'Auth.form.error.code.invalid',
              message: 'Invalid Code or Number',
              field: ['mobileNumber'],
            })
          );
        }
    
     let updateData = {
          token: '',
          confirmed: true
        };
    
    
        const data = await strapi.plugins['users-permissions'].services.user.edit({ id }, updateData);
        const jwt = strapi.plugins['users-permissions'].services.jwt.issue({
          id: data.id,
        })
        ctx.send({ jwt, user: sanitizeUser(data) });
      }
  }

