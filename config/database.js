module.exports = ({ env }) => ({
  connection: {
    client: 'postgres',
    connection:{ 
      host: env('DATABASE_HOST', 'localhost'),
      port: env.int('DATABASE_PORT', 5432),
      database: env('DATABASE_NAME', 'strapi-db-name'),
      user: env('DATABASE_USERNAME', 'strapi-db-user'),
      password: env('DATABASE_PASSWORD', 'your-strapi-db-password'),
      ssl: env.bool('DATABASE_SSL', false),
      timezone: "Asia/Calcutta"
    },
  },
});
