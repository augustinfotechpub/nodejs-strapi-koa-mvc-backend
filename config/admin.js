module.exports = ({ env }) => ({
  auth: {
    secret: env('ADMIN_JWT_SECRET', 'update-your-JWT-here'),
  },
  watchIgnoreFiles:[
    '**/*.csv'
  ]
});
