/*
 *
 * HomePage
 *
 */

import React, { memo, useState, useEffect } from "react";
// import PropTypes from 'prop-types';
import pluginId from '../../pluginId';
//import Loader from "../../components/Loader";
import adminReportRequests from "../../api/report";
import { LoadingIndicatorPage } from "@strapi/helper-plugin"
import "../../styles/HomePage/index.css";
import ReactPaginate from "react-paginate";
//import FilterModal from "../../components/FilterModal";
import {
  Table,
  Thead,
  Tbody,
  Tr,
  Td,
  Th,
  Typography,
  Avatar,
  Button,
  ModalLayout,
  ModalBody,
  ModalFooter,
  NumberInput,
  TextInput,
  Select,
  Option,
  Tooltip,
  Information,
  DatePicker,
  BaseCheckbox,
  Box,
} from '@strapi/design-system';
import { Dots, NextLink, PageLink, Pagination, PreviousLink } from '@strapi/design-system';
import { Layout, BaseHeaderLayout, ContentLayout, } from "@strapi/design-system/Layout";
import { Plus, Filter, Cross, Download } from '@strapi/icons';


const HomePage = () => {
  //const currentPage= 1;
  const [name, setName] = useState("");
  const [value, setValue] = useState("inward_date");
  const [value2, setValue2] = useState("Is");
  const [content, setContent] = useState('');
  const [filterList, setFilterList] = useState([]);
  const [pagination, setPagination] = useState(10);
  const [fromDate, setFromDate] = useState(new Date());
  const [toDate, setToDate] = useState(new Date());
  const [numberDisable, setNumberDisable] = useState(false);
  const [error, toggleError] = useState();
  const [isActive, setIsActive] = useState(true);
  const [isLoading, setIsLoading] = useState(true);
  const ROW_COUNT = 10;
  const COL_COUNT = 12;
  const [totalPage, setTotalPage] = useState(0);
  const [totalEntries, setTotalEntries] = useState(0)
  const [currentStart, setCurrentStart] = useState(0);
  const [showModal, setShowModal] = useState(false);
  const [reportData, setReportData] = useState([]);
  // const [currentLimit,setCurrentLimit]=useState(10);
  const [currentQuery, setCurrentQuery] = useState({});
  const [number, setNumber] = useState(1); // No of pages in table
  // const lastPost = number * pagination;
  // const firstPost = lastPost - pagination;
  // let currentPost ;
  let PageCount;
  const ChangePage = async ({ selected }) => {
    // console.log(" active page", selected +
    // if (!selected == 0) {
    //   console.log("list", list);

    //   let list = document.querySelector('[aria-label="Page 1"]');
    //   if (list) {
    //     list.setAttribute('class', ' ');
    //   }
    // }

    // let active = document.querySelector(`[aria-label="Page ${number} is your current page"]`);
    // console.log("active", active);

    // if (active) {
    //   active.setAttribute('class', ' ');
    // }
    const startData = (selected * pagination);
    const limitData = pagination
    setIsActive(true)
    //setIsLoading(true)
    const filterData = await queryFilter();
    const pageData = await getInwardData(startData, limitData, filterData);
    setReportData(pageData);
    // const sel = selected + 1;
    // setNumber(sel);

    // console.log("chasojfnhdohgsd", filterList.length);

  };

  const handleSubmit = async (e) => {
    // Prevent submitting parent form
    e.preventDefault();
    e.stopPropagation();
    setShowModal(false)
    console.log(e.detail.value , "value");
    // if(e.detail.value)
    if (value == "material_category") {
      setFilterList(filterListData => [...filterListData, { name: "Material Category", dbName: "material_category", value: content }]);
    }
    if (value == "material_item") {
      setFilterList(filterListData => [...filterListData, { name: "Material Item", dbName: "material_item", value: content }]);
    }
    if (value == "project_status") {
      setFilterList(filterListData => [...filterListData, { name: "Project Status", dbName: "project_status", value: content }]);
    }
    if (value == "material_brand") {
      setFilterList(filterListData => [...filterListData, { name: "Material Brand", dbName: "material_brand", value: content }]);
    }
    if (value == "inward_date") {
      setFilterList(filterListData => [...filterListData, { name: "Inward Date", dbName: "inward_date", value: fromDate, value2: toDate }]);
    }
    if (value == "state") {
      setFilterList(filterListData => [...filterListData, { name: "State", dbName: "state_id", value: content }]);
    }
    if (value == "district") {
      setFilterList(filterListData => [...filterListData, { name: "District", dbName: "district_id", value: content }]);
    }
    if (value == "city") {
      setFilterList(filterListData => [...filterListData, { name: "City", dbName: "city_id", value: content }]);
    }
  };

  const deleteButton = (index) => {
    setFilterList([
      ...filterList.slice(0, index),
      ...filterList.slice(index + 1, filterList.length)
    ]);
  };


  const queryFilter = async () => {
    const filterQueryData = []
    const filterProjectData = []
    filterList.forEach((fList) => {
      if (fList.dbName == "material_category") {
        let fObject = {
          material_category: {
            material_category_name: { $eq: fList.value },
          }
        }
        filterQueryData.push(fObject);
      }
      if (fList.dbName == "project_status") {
        let fObject = {
          project: {
            project_status: { $eq: fList.value },
          }
        }
        filterQueryData.push(fObject);
      }
      if (fList.dbName == "material_item") {
        let fObject = {
          material_item: {
            material_item_name: { $eq: fList.value },
          }
        }
        filterQueryData.push(fObject);
      }
      if (fList.dbName == "material_brand") {
        let fObject = {
          material_brand: {
            material_brand_name: { $eq: fList.value },
          }
        }
        filterQueryData.push(fObject);
      }
      if (fList.dbName == "inward_date") {
        let formatFromDate = formatDate(fList.value)
        let formatToDate = formatDate(fList.value2)
        let fObject = {
          inward_date: {
            $gte: formatFromDate
          }
        }
        filterQueryData.push(fObject);
        let tObject = {
          inward_date: {
            $lte: formatToDate
          }
        }
        filterQueryData.push(tObject);
      }
      if (fList.dbName == "state_id") {
        let projectFilterList = {
          state_id: {
            state_name: { $eq: fList.value }
          }
        }
        filterProjectData.push(projectFilterList);
      }
      if (fList.dbName == "district_id") {
        let projectFilterList = {
          district_id: {
            district_name: { $eq: fList.value }
          }
        }
        filterProjectData.push(projectFilterList);
      }
      if (fList.dbName == "city_id") {
        let projectFilterList = {
          city_id: {
            city_name: { $eq: fList.value }
          }
        }
        filterProjectData.push(projectFilterList);
      }
    })
    if (filterProjectData.length > 0) {
      const projectDataForFilters = await adminReportRequests.getProjectByFilter(filterProjectData)
      const projectFilterID = [...new Set(projectDataForFilters.map(q => q?.id))];
      let fObject = {
        project: {
          id: { $in: projectFilterID },
        }
      }
      filterQueryData.push(fObject);
    }
    return filterQueryData
  }
  const getError = () => {
    // Form validation error

    if (name.length > 40) {
      return "Content is too long";
    }

    return null;
  };
  const formatDate = (date) => {
    let day = (new Date(date)).toLocaleDateString('en-US', { day: 'numeric' });
    let year = (new Date(date)).toLocaleDateString('en-US', { year: 'numeric' });
    let month = (new Date(date)).toLocaleDateString('en-US', { month: 'numeric' });

    if (month.length < 2) {
      month = '0' + month;
    }
    if (day.length < 2) {
      day = '0' + day;
    }

    return [year, month, day].join('-');
  }
  const formatDateCsv = (date) => {
    let day = (new Date(date)).toLocaleDateString('en-US', { day: 'numeric' });
    let year = (new Date(date)).toLocaleDateString('en-US', { year: 'numeric' });
    let month = (new Date(date)).toLocaleDateString('en-US', { month: 'numeric' });

    if (month.length < 2) {
      month = '0' + month;
    }
    if (day.length < 2) {
      day = '0' + day;
    }

    return [day, month, year].join('-');
  }


  const getInwardData = async (start, limit, query) => {
    const qs = {
      start: start,
      limit: limit,
      query: query
    }
    const inwardData = await adminReportRequests.getAllInwards(qs);
    PageCount = Math.ceil(inwardData.meta.total / pagination);
    setTotalPage(PageCount)
    setTotalEntries(inwardData.meta.total)
    let reportData = inwardData.data;
    const projectID = [...new Set(reportData.map(q => q?.project?.id))];
    const projectData = await adminReportRequests.getProjects(projectID);
    reportData.forEach((report) => {
      projectData.forEach((project) => {
        if (report?.project?.id == project?.id) {
          report.stateName = project?.state_id?.state_name;
          report.districtName = project?.district_id?.district_name;
          report.cityName = project?.city_id?.city_name;
        }
      })
    })
    setIsLoading(false)
    setIsActive(false)
    return reportData;
  }
  const getAllInwardDataForCsv = async (query) => {
    const inwardData = await adminReportRequests.getInwardForCsv(query);
    const projectID = [...new Set(inwardData.map(q => q?.project?.id))];
    const projectData = await adminReportRequests.getProjects(projectID);
    inwardData.forEach((report) => {
      projectData.forEach((project) => {
        if (report?.project?.id == project?.id) {
          report.stateName = project?.state_id?.state_name;
          report.districtName = project?.district_id?.district_name;
          report.cityName = project?.city_id?.city_name;
        }
      })
    })
    return inwardData;
  }


  // useEffect(async () => {
  //   await getInwardData(currentStart,pagination,currentQuery);
  // }, []);
  useEffect(() => {
    if (value === "inward_date") {
      setNumberDisable(true);
    }
    else {
      setNumberDisable(false);
    }
  }, [value])
  useEffect(async () => {
    const filterData = await queryFilter();
    setIsActive(true)
    //setIsLoading(true)
    let reportData = await getInwardData(currentStart, pagination, filterData);
    setReportData(reportData);
  }, [filterList])


  useEffect(() => {
    const firstPaginationButton = document.querySelector('[aria-label="Page 1"]');
    if (firstPaginationButton) {
      firstPaginationButton.click();
    }

  }, [filterList.length]);


  const SavedcsvData = async () => {
    const filterData = await queryFilter();
    let csvData = await getAllInwardDataForCsv(filterData);
    let xy =
      `Title:Inward Report`
    xy = xy + "\n" + `S.No,Inward Date,Workspace,Project,Project Status,State,District,City,Material Category,Material Item,Material Brand,Inward Quantity`
    csvData.map((item, index) => {
      let serial = index + 1;
      let date = formatDateCsv(item.inward_date)
      xy = xy + "\n" + [serial, date, item.workspace?.workspace_name, item.project?.project_name, item.project?.project_status, item?.stateName, item?.districtName, item?.cityName, item?.material_category?.material_category_name, item?.material_item?.material_item_name
        , item?.material_brand?.material_brand_name, item?.inward_quantity]
      xy = xy
    })
    const blob = new Blob([xy], { type: 'text/plain' });
    const url = window.URL.createObjectURL(blob);
    const link = document.createElement('a');
    if (link.download !== undefined) { // feature detection
      link.setAttribute('href', url);
      link.setAttribute('download', 'inwardreport.csv');
      link.style.visibility = 'hidden';
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    }
  }
  if (isLoading) return <LoadingIndicatorPage />;
  return (
    <Layout>
      <ContentLayout>
        {/* <Button  onClick={() => setShowModal(true)} variant='secondary'>Filter</Button> */}
        <Box padding={8} background="neutral100">
          <div style={{
            background: "#f6f6f9",
            paddingTop: "20px",
            paddingBottom: "40px",
          }}>
            <div style={{
              color: "#32324d",
              fontWeight: 600,
              fontSize: "2rem",
              lineHeight: 1.25,
            }}>Inward Report</div>
            <div style={{
              color: "#666687",
              fontSize: " 1rem",
              lineHeight: 1.5,
            }}>{totalEntries} entries</div>

          </div>

          <div style={{ display: "flex", justifyContent: "space-between" }}>
            <div style={{ display: "flex", gap: '5px', flexWrap: "wrap", width: "100%" }}>
              <Button onClick={() => setShowModal(true)} startIcon={<Filter />} variant='secondary' style={{ marginBottom: "10px", }} >Filter</Button>
              {filterList.length ? filterList.map((element, index) => {
                if (element.dbName != "inward_date") {
                  return <Button key={index} variant='secondary' endIcon={<Cross />} onClick={() => deleteButton(index)} style={{ marginBottom: "10px", display: "flex", justifyContent: "flex-start", }} >{element.name} {value2} {element.value} | </Button>
                } else {
                  const from = element.value.toString().split("00:00:00", 1);
                  const to = element.value2.toString().split("00:00:00", 1);
                  return <Button key={index} variant='secondary' endIcon={<Cross />} onClick={() => deleteButton(index)} style={{ marginBottom: "10px", display: "flex", justifyContent: "flex-start", }} >{element.name} {value2} {from} - {to} | </Button>
                }
              }) : null}

            </div>
            <div style={{ display: "flex", justifyContent: "flex-end", alignSelf: "flex-end" }}><Button variant='secondary' style={{ marginBottom: "10px", justifyContent: "flex-end" }} onClick={SavedcsvData} startIcon={<Download />}>Export</Button></div>
          </div>
          <Table colCount={COL_COUNT} rowCount={ROW_COUNT} >

            <Thead>
              <Tr>
                <Th>
                  <Typography variant="sigma">ID</Typography>
                </Th>
                <Th>
                  <Typography variant="sigma">Date</Typography>
                </Th>
                <Th>
                  <Typography variant="sigma">Workspace</Typography>
                </Th>
                <Th>
                  <Typography variant="sigma">Project</Typography>
                </Th>
                <Th>
                  <Typography variant="sigma">Project Status</Typography>
                </Th>
                <Th>
                  <Typography variant="sigma">State</Typography>
                </Th>
                <Th>
                  <Typography variant="sigma">District</Typography>
                </Th>
                <Th>
                  <Typography variant="sigma">City</Typography>
                </Th>
                <Th>
                  <Typography variant="sigma">Material Category</Typography>
                </Th>
                <Th>
                  <Typography variant="sigma">Material Item</Typography>
                </Th>
                <Th>
                  <Typography variant="sigma">Material Brand</Typography>
                </Th>
                <Th>
                  <Typography variant="sigma">Inward Quantity</Typography>
                </Th>
              </Tr>
            </Thead>
            <Tbody>
              {isActive ?
                <Tr >
                  <Td>
                    <Typography textColor="neutral800"></Typography>
                  </Td>
                  <Td>
                    <Typography textColor="neutral800"></Typography>
                  </Td>
                  <Td>
                    <Typography textColor="neutral800"></Typography>
                  </Td>
                  <Td>
                    <Typography textColor="neutral800"></Typography>
                  </Td>
                  <Td>
                    <Typography textColor="neutral800"></Typography>
                  </Td>
                  <Td>
                    <Typography textColor="neutral800"></Typography>
                  </Td>
                  <Td>
                    <Typography textColor="neutral800"></Typography>
                  </Td>
                  <Td><LoadingIndicatorPage /></Td>

                </Tr>
                :
              
                  
                    reportData && reportData.length != 0 ? ( reportData.map(reportData => <Tr key={reportData.id}>
                      <Td>
                        <Typography textColor="neutral800">{reportData.id}</Typography>
                      </Td>
                      <Td>
                        <Typography textColor="neutral800">{formatDateCsv(reportData.inward_date)}</Typography>
                      </Td>
                      <Td>
                        <Typography textColor="neutral800">{reportData?.workspace?.workspace_name ? reportData?.workspace?.workspace_name : "-"}</Typography>
                      </Td>
                      <Td>
                        <Typography textColor="neutral800">{reportData?.project?.project_name ? reportData?.project?.project_name : "-"}</Typography>
                      </Td>
                      <Td>
                        <Typography textColor="neutral800">{reportData?.project?.project_status ?reportData?.project?.project_status : "-"}</Typography>
                      </Td>
                      <Td>
                        <Typography textColor="neutral800">{reportData?.stateName ? reportData?.stateName : "-"}</Typography>
                      </Td>
                      <Td>
                        <Typography textColor="neutral800">{reportData?.districtName ? reportData?.districtName : "-"}</Typography>
                      </Td>
                      <Td>
                        <Typography textColor="neutral800">{reportData?.cityName ? reportData?.cityName : "-"} </Typography>
                      </Td>
                      <Td>
                        <Typography textColor="neutral800">{reportData?.material_category?.material_category_name ? reportData?.material_category?.material_category_name :"-"}</Typography>
                      </Td>
                      <Td>
                        <Typography textColor="neutral800">{reportData?.material_item?.material_item_name ? reportData?.material_item?.material_item_name : "-"}</Typography>
                      </Td>
                      <Td>
                        <Typography textColor="neutral800">{reportData?.material_brand?.material_brand_name ? reportData?.material_brand?.material_brand_name : "-"}</Typography>
                      </Td>
                      <Td>
                        <Typography textColor="neutral800">{reportData?.inward_quantity ? reportData?.inward_quantity : "-"}</Typography>
                      </Td>
                    </Tr>
                    )
                  
                )
                :
                <Tr style = {{position:"fixed",
                  top: "50%",
                  left: "36%",
                  height:"50vh",width:"600px",display:"flex",justifyContent:"center",alignItem:"center",color:"white",fontSize:"20px"}}

                //  style = {{height:"50vh",width:"600px",display:"flex",justifyContent:"center",alignItem:"center",color:"white",fontSize:"20px"}}
                 >
                   <Td>

                   No data found
                   </Td>
                   
                </Tr>
 
              }
            </Tbody>
          </Table>
          <div style={{ display: "flex", justifyContent: "space-between" }}>
            <div style={{ width: "100px", paddingTop: "20px" }}>
              {/* <Select style={{ width: "100%", height: "80%" }} aria-label="pagination" error={error} value={pagination} onChange={setPagination} placeholder={"10"}>
                      <Option value="10">10</Option>
                      <Option value="20">20</Option>
                      <Option value="50">50</Option>
                      <Option value="100">100</Option>
                    </Select> */}
            </div>
            <div style={{ display: "flex", justifyContent: "flex-end", paddingTop: "20px" }}>
              <ReactPaginate
                previousLabel={"Previous"}
                nextLabel={"Next"}
                pageCount={totalPage}
                onPageChange={ChangePage}
                disableInitialCallback={true}
                containerClassName={"paginationBttns"}
                activeClassName={"paginationBttnsActive"}
                initialPage={0}
              ></ReactPaginate>
            </div>
          </div>
        </Box>

      </ContentLayout>

      {
        showModal &&
        <ModalLayout
          style={{ height: "auto", width: "300px", left: "320px", top: "210px", position: "absolute" }}

          onClose={() => setShowModal(false)}
          labelledBy="title"
          as="form"

        >
          <ModalBody
            style={{ width: "100%", }}
          >
            <Select id="select1" aria-label="Choose your filter" error={error} value={value} onChange={setValue} selectButtonTitle="Carret Down Button"
            >
              <Option value="inward_date">Inward Date</Option>
              <Option value="state">State</Option>
              <Option value="district">District</Option>
              <Option value="city">City</Option>
              <Option value="project_status">Project Status</Option>
              <Option value="material_category">Material Category</Option>
              <Option value="material_item">Material Item</Option>
              <Option value="material_brand">Material Brand</Option>

            </Select>
            <br></br>
            <Select id="select2" aria-label="Choose your filter" error={error} value={value2} onChange={setValue2} selectButtonTitle="Carret Down Button"
            >
              <Option value="Is">Is</Option>
              {/* <Option value="Is Not">Is Not</Option>
              <Option value="Is Null">Is Null</Option>
              <Option value="Is Not Null">Is Not Null</Option>
              <Option value="Is Greater than">Is Greater than</Option>
              <Option value="Is Greater Than or Equal to">Is Greater Than or Equal to</Option> */}

            </Select>
            <br></br>
            {numberDisable ? <div>

              <DatePicker onChange={setFromDate} label="From Date" selectedDate={fromDate} name="datepicker" aria-label="Date" clearLabel="Clear the datepicker" onClear={() => setFromDate(undefined)} selectedDateLabel={formattedDate => `Date picker, current is ${formattedDate}`}
              />
              <br></br>
              <DatePicker onChange={setToDate} label="To Date" selectedDate={toDate} name="datepicker" aria-label="Date" clearLabel="Clear the datepicker" onClear={() => setToDate(undefined)} selectedDateLabel={formattedDate => `Date picker, current is ${formattedDate}`}
              />
              <br></br>
            </div>
              :
              <TextInput aria-label="Content" disabled={numberDisable} name="content" error={error} onChange={e => setContent(e.target.value)} value={content}
              />
            }


          </ModalBody>

          <ModalFooter
            startActions={<Button variant='secondary' startIcon={<Plus />} size="M" type="button" onClick={handleSubmit} style={{ width: "260px" }} fullWidth>Add Filter</Button>
            }

          />
        </ModalLayout>
      }

    </Layout>

  );
}
export default HomePage;
