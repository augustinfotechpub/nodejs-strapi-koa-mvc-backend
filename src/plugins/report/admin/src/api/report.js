import { request } from "@strapi/helper-plugin";

const adminReportRequests = {
    getAllInwards: async (qs) => {
        const {data,meta}= await request("/report/find", {
          method: "POST",
          body:{data:qs}
        });
        return {data,meta}
        //console.log("report data"+reportdata)
      },
    getProjects: async (data) => {
        return await request(`/report/getproject`, {
            method: "POST",
            body: { data: data },
        });
    },
    getProjectByFilter: async (fs) => {
        const projectData= await request(`/report/getprojectbyfilter`, {
            method: "POST",
            body: { data: fs },
        });
        return projectData
    },
    getInwardForCsv: async (fs) => {
        const csvData= await request(`/report/getinwardforcsv`, {
            method: "POST",
            body: { data: fs },
        });
        return csvData
    },
}

export default adminReportRequests;