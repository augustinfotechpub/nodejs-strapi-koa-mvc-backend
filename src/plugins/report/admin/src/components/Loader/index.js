import React from "react";
import { LoaderWrapper } from "../../styles/Loader";
import { LoadingOutlined } from "@ant-design/icons";
import { Spin } from "antd";

const antIcon = <LoadingOutlined style={{ fontSize: 45 }} spin />;

const Loader = () => {
    return (
        <LoaderWrapper>
            <Spin indicator={antIcon} />
        </LoaderWrapper>
    );
};

export default Loader;
