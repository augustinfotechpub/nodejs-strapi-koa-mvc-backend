import React, { useState } from "react";

import {
  ModalLayout,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Typography,
  Button,
  TextInput,
  Select,
  Option,
} from "@strapi/design-system";

export default function FilterModal({ setShowModal, addTodo }) {
  const [name, setName] = useState("");
  const [value, setValue] = useState();
  const [error, toggleError] = useState();
  const [disabled, toggleDisabled] = useState();
  const handleSubmit = async (e) => {
    // Prevent submitting parent form
    e.preventDefault();
    e.stopPropagation();

    try {
      await addTodo({ name: name });
      setShowModal(false);
    } catch (e) {
      console.log("error", e);
    }
  };

  const getError = () => {
    // Form validation error

    if (name.length > 40) {
      return "Content is too long";
    }

    return null;
  };

  return (
    <ModalLayout
      onClose={() => setShowModal(false)}
      labelledBy="title"
      as="form"
      onSubmit={handleSubmit}
    >
      {/* <ModalHeader>
        <Typography fontWeight="bold" textColor="neutral800" as="h2" id="title">
          Add Filter
        </Typography>
      </ModalHeader> */}

      <ModalBody>
          <Select id="select1" aria-label="Choose your meal" placeholder="Your example"  error={error} value={value} onChange={setValue} disabled={disabled}  selectButtonTitle="Carret Down Button">
            <Option value="pizza">Pizza</Option>
            <Option value="hamburger">Hamburger</Option>
            <Option value="bagel">Bagel</Option>
          </Select>
          <Select id="select1" aria-label="Choose your meal" placeholder="Your example"  error={error} value={value} onChange={setValue} disabled={disabled}  selectButtonTitle="Carret Down Button">
            <Option value="Is">Is</Option>
            
          </Select>
          <TextInput
              aria-label="Choose your meal"
              name="text"
              error={getError()}
              onChange={(e) => setName(e.target.value)}
              value={name}
            />
      </ModalBody>

      {/* <ModalFooter
        startActions={
          <Button onClick={() => setShowModal(false)} variant="tertiary">
            Cancel
          </Button>
        }
        endActions={<Button type="submit">Add Filter</Button>}
      /> */}
    </ModalLayout>
  );
}