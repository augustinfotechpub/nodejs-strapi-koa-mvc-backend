'use strict';

const myService = require('./my-service');
const report = require('./report')
module.exports = {
  myService,
  report,
};
