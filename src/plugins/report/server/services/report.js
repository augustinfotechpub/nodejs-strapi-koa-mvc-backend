"use strict";

module.exports = ({ strapi }) => ({
  // Method 1: Creating an entirely new custom service
  async exampleAction(...args) {
    let response = { okay: true }
    if (response.okay === false) {
      return { response, error: true }
    }

    return response
  },
  async find(query) {
    const meta={total:0,page:0,pageSize:0,pageCount:0}
    const total=await strapi.entityService.findMany("api::inward-entry.inward-entry", {
        populate: '*',
        filters: {
            $and:query.data.query
        },
    });
    const data = await strapi.entityService.findMany("api::inward-entry.inward-entry", {
        populate: '*',
        filters: {
            $and:query.data.query
        },
        sort: 'id',
        start:query.data.start,
        limit:query.data.limit
        
      });
    meta.page=query.data.start;
    meta.pageCount=data.length;
    meta.pageSize=query.data.limit;
    meta.total=total.length;
    return { data, meta}
  },
  async getproject(query) {
    return await strapi.entityService.findMany("api::project.project", {
        populate: '*',
        filters: {
            id: {
              $in: query.data,
            },
          },
        
      });
  },
  async getprojectbyfilter(query) {
    const report= await strapi.entityService.findMany("api::project.project", {
        populate: '*',
        filters: {
            $and:query.data
        },
      });
    return report
  },
  async getinwardforcsv(query) {
    const report= await strapi.entityService.findMany("api::inward-entry.inward-entry", {
        populate: '*',
        filters: {
            $and:query.data
        },
    });
    return report
  },
});