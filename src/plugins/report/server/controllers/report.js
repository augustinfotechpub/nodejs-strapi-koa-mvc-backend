'use strict';
module.exports = {
   async find(ctx) {
    try {
        const { data, meta }= await strapi.plugin("report").service("report").find(ctx.request.body);
        return {data,meta};
    } catch (err) {
      ctx.throw(500, err);
    }
  },
  async getproject(ctx) {
    try {
      return await strapi.plugin("report").service("report").getproject(ctx.request.body);
    } catch (err) {
      ctx.throw(500, err);
    }
  },
  async getprojectbyfilter(ctx) {
    try {
      return await strapi.plugin("report").service("report").getprojectbyfilter(ctx.request.body);
    } catch (err) {
      ctx.throw(500, err);
    }
  },
  async getinwardforcsv(ctx) {
    try {
      return await strapi.plugin("report").service("report").getinwardforcsv(ctx.request.body);
    } catch (err) {
      ctx.throw(500, err);
    }
  },
};