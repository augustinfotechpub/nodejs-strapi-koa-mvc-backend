'use strict';

const myController = require('./my-controller');
const report = require('./report');

module.exports = {
  myController,
  report,

};
