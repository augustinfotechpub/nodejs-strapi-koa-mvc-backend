module.exports = [
  {
    method: 'GET',
    path: '/',
    handler: 'myController.index',
    config: {
      policies: [],
    },
  },
  {
    method: "POST",
    path: "/find",
    handler: "report.find",
    config: {
      policies: [],
    },
  },
  {
    method: "POST",
    path: "/getproject",
    handler: "report.getproject",
    config: {
      policies: [],
    },
  },
  {
    method: "POST",
    path: "/getprojectbyfilter",
    handler: "report.getprojectbyfilter",
    config: {
      policies: [],
    },
  },
  {
    method: "POST",
    path: "/getinwardforcsv",
    handler: "report.getinwardforcsv",
    config: {
      policies: [],
    },
  },

];
