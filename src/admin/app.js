import Logo from './extensions/csm-logo.png';
export default {
  config: {
    locales: [
      // 'ar',
       'fr',
      // 'cs',
      // 'de',
      // 'dk',
      // 'es',
      // 'he',
      // 'id',
      // 'it',
      // 'ja',
      // 'ko',
      // 'ms',
      // 'nl',
      // 'no',
      // 'pl',
      // 'pt-BR',
      // 'pt',
      // 'ru',
      // 'sk',
      // 'sv',
      // 'th',
      // 'tr',
      // 'uk',
      // 'vi',
      // 'zh-Hans',
      // 'zh',
    ],
    auth:{
      logo:Logo,
    },
    menu:{
      logo:Logo,
    },
    head: {
      favicon: Logo,
    },
    translations: {
      en: {
        'Auth.form.welcome.title': 'Welcome to CSM',
        'Auth.form.welcome.subtitle':'Log in to your CSM account',
        'app.components.LeftMenu.navbrand.title':'CSM Dashboard'
      },
    }

  },
  bootstrap(app) {
    console.log(app);
  },
};
