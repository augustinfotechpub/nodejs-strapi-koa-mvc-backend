'use strict';

/* eslint-disable no-unused-vars */
module.exports = {
  // WARNING: the admin panel now uses webpack 5 to bundle the application.
  webpack: (config, webpack) => {
    // Note: we provide webpack above so you should not `require` it

    // Perform customizations to webpack config
    config.plugins.push(new webpack.IgnorePlugin(/\/__tests__\//));

    // Important: return the modified config
    return config;
  },
};
