'use strict';
const _ = require('lodash');
const utils = require('@strapi/utils');
const { convertPopulateQueryParams } = require('@strapi/utils/lib/convert-query-params');
const { getService } = require('./utils');
const { ApplicationError, ValidationError } = utils.errors;
const { validateCreateUserBody, validateUpdateUserBody } = require('@strapi/plugin-users-permissions/server/controllers/auth');
const { sanitize } = utils;
const { callback } = require("./controllers/auth.js");
const sanitizeOutput = (user, ctx) => {
    const schema = strapi.getModel('plugin::users-permissions.user');
    const { auth } = ctx.state;
    return sanitize.contentAPI.output(user, schema, { auth });
  };
 
module.exports = (plugin) => {
  
  plugin.controllers.auth.callback = async (ctx) => {
    try {
      await callback(ctx);
      // ctx.send(result);
    } catch (error) {
      throw new ApplicationError(error.message);
    }
  };

 
  //  plugin.controllers.user['create']= async (ctx) =>{

  //     const advanced = await strapi
  //       .store({ type: 'plugin', name: 'users-permissions', key: 'advanced' })
  //       .get();
  
  //     // await validateCreateUserBody(ctx.request.body);
  //     console.log(ctx.request.body  , "ctx");
  //     const { email, username, role } = ctx.request.body;
    


  //     console.log(email , "email" , username , "username");
  //     const userWithSameUsername = await strapi
  //       .query('plugin::users-permissions.user')
  //       .findOne({ where: { username } });
  
  //     if (userWithSameUsername) {
  //       if (!email) throw new ApplicationError('Username already taken');
  //     }
  
  //     if (advanced.unique_email) {
  //       const userWithSameEmail = await strapi

  //         .query('plugin::users-permissions.user')
  //         .findOne({ where: { email: email.toLowerCase() } });  
  //       if (userWithSameEmail) {
  //         throw new ApplicationError('Email already taken');
  //       }
  //     }
  
  //     const user = {
  //       ...ctx.request.body,
  //       provider: 'local',
  //     };
  
  //     user.email = _.toLower(user.email);
  
  //     if (!role) {
  //       const defaultRole = await strapi
  //         .query('plugin::users-permissions.role')
  //         .findOne({ where: { type: advanced.default_role } });
  
  //       user.role = defaultRole.id;
  //     }
  
  //    try {
  //       const data = await getService('user').add(user);
  //       const sanitizedData = await sanitizeOutput(data, ctx);
  
  //       ctx.created(sanitizedData);
  //      } 
  //     catch (error) {
  //       throw new ApplicationError(error);

  //     }
  //       let options = ctx.request.body
  //     console.log("options",options.mobileNumber);
  //     const Sendgrid = await strapi.plugins['email'].services.email.send({
  //         to:options.email,
  //         from:'devarshi.vaidya@augustinfotechteam.com',
  //         replyTo: 'devarshi.vaidya@augustinfotechteam.com',
  //         subject: "Test email",
  //         text:`
  //           Your otp :  

  //             Following are details that has filed by user
  //             First Name : ${options.firstName || "Not Field by user"}
  //             Last Name :  ${options.lastName || "Not Field by user"}
  //             User Name: ${options.username || "Not Field by user"}
  //             Email:  ${options.email || "Not Field by user"}
  //             Mobile Number: ${options.mobileNumber|| "Not Field by user"}          
  //         `,
  //     });
  //     ctx.send({
  //       user: user
  //     });

  //   //   const accountSid = process.env.Account_SID;
  //   //   const authToken = process.env.Auth_Token;

  //   //   const token = Math.floor(Math.random() * 90000) + 10000;

  //   //   const client = require('twilio')(accountSid, authToken);
  //   // console.log(client.messages , "client");

  //   //       client.messages
  //   //   .create({
  //   //     body: `Your OTP is - ${token}`,
  //   //     from: process.env.Twilio_phone_number, //the phone number provided by Twillio
  //   //     to:"+919106716144"
  //   //   })
  //   //   .then(message => console.log(message.sid)
  //   //   .done());

  //   //   //to send the data to otp verification
  //   //   const postData = {
  //   //     data :{
  //   //       Mobile_Number: options.mobileNumber,
  //   //       Genrate_OTP: token,
  //   //     }
  //   //   }

  //   //   let entity = 'api::otp.otp'
  //   //   strapi.query(entity).create( postData);
  //   //   // const created = await strapi.services['api::otp.otp'].create(postData);     
  //   // return ctx.send(Sendgrid);
  //  }

   
//    plugin.controllers.user['find']= async (ctx) =>{
//     console.log("inside find" ,ctx.query.filters);
//   const users = await getService('user').fetchAll(
//     ctx.query.filters,
//     convertPopulateQueryParams(ctx.query.populate || {})
//   );

//   ctx.body = await Promise.all(users.map(user => sanitizeOutput(user, ctx)));
// }
    return plugin;

}