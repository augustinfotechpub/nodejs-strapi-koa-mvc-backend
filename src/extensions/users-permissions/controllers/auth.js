const _ = require('lodash');
const utils = require('@strapi/utils');
const { convertPopulateQueryParams } = require('@strapi/utils/lib/convert-query-params');
const { getService } = require('../utils');
const { ApplicationError, ValidationError } = utils.errors;
const phoneNumberRegExp = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/;
const emailRegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const { sanitize } = utils;
const sanitizeOutput = (user, ctx) => {
    const schema = strapi.getModel('plugin::users-permissions.user');
    const { auth } = ctx.state;
  
    return sanitize.contentAPI.output(user, schema, { auth });
  };

module.exports = {
  async callback(ctx) {
    const provider = ctx.params.provider || 'local';
      const params = ctx.request.body;
      const store = await strapi.store({ type: 'plugin', name: 'users-permissions' });
      if (provider === 'local') {
        // if (!_.get(await store.get({ key: 'grant' }), 'email.enabled')) {
        //   throw new ApplicationError('This provider is disabled');
        // }
  
       // await validateCallbackBody(params);
  
        const query = { provider };
  
        // Check if the provided identifier is an email or not.
        const isEmail = emailRegExp.test(params.identifier);
        const isMobileNumber = phoneNumberRegExp.test(params.identifier);
  
        // Set the identifier to the appropriate query field.
        if (isEmail) {
          query.email = params.identifier.toLowerCase();
        } else if (isMobileNumber) {
          query.mobileNumber = params.identifier;
        } else {
          query.username = params.identifier;
        }
  
        // Check if the user exists.
        const user = await strapi.query('plugin::users-permissions.user').findOne({ where: query });
  
        if (!user) {
          throw new ValidationError('Invalid identifier or password');
        }
  
        // if (
        //   _.get(await store.get({ key: 'advanced' }), 'email_confirmation') &&
        //   user.confirmed !== true
        // ) {
        //   throw new ApplicationError('Your account email is not confirmed');
        // }
  
        if (user.blocked === true) {
          throw new ApplicationError('Your account has been blocked by an administrator');
        }
  
        // The user never authenticated with the `local` provider.
        if (!user.password) {
          throw new ApplicationError(
            'This user never set a local password, please login with the provider used during account creation'
          );
        }
        const validPassword = await getService('user').validatePassword(
          params.password,
          user.password
        );
  
        if (!validPassword) {
          throw new ValidationError('Invalid identifier or password');
        } else {
          ctx.send({
            jwt: getService('jwt').issue({
            //  id: user.id,
            }),
            user: user
            //user: sanitizeOutput(user, ctx),
          });
        }
      } 
       else {
        // if (!_.get(await store.get({ key: 'grant' }), [provider, 'enabled'])) {
        //   throw new ApplicationError('This provider is disabled');
        // }
  
        // Connect the user with the third-party provider.
        let user;
        let error;
        try {
          [user, error] = await getService('providers').connect(provider, ctx.query);
        } catch ([user, error]) {
          throw new ApplicationError(error.message);
        }
  
        if (!user) {
          throw new ApplicationError(error.message);
        }
  
        ctx.send({
            jwt: getService('jwt').issue({
            //  id: user.id,
            }),
            user: user
            //user: sanitizeOutput(user, ctx),
          });
      }
  },
};