'use strict';

/**
 * aac-width router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::aac-width.aac-width');
