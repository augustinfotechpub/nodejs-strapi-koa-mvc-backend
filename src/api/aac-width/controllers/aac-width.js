'use strict';

/**
 *  aac-width controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::aac-width.aac-width');
