'use strict';

/**
 * aac-width service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::aac-width.aac-width');
