'use strict';

/**
 * inward-entry service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::inward-entry.inward-entry');
