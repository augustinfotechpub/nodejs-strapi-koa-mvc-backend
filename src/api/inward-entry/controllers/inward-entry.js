'use strict';

/**
 *  inward-entry controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::inward-entry.inward-entry');
