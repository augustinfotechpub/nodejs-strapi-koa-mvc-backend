'use strict';

/**
 * inward-entry router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::inward-entry.inward-entry');
