'use strict';

/**
 * material-category service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::material-category.material-category');
