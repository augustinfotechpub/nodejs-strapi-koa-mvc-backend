'use strict';

/**
 * material-category router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::material-category.material-category');
