'use strict';

/**
 *  material-category controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::material-category.material-category');
