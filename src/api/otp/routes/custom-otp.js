module.exports = {
    routes: [
      {
        method: 'POST',
        path: '/otps/createotp',
        handler: 'otp.createotp',
        config: {
            auth: false,
          },
      },
    ],
};