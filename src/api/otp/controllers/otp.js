'use strict';

/**
 *  otp controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

const accountSid = process.env.Account_SID;
const authToken = process.env.Auth_Token;
const client = require('twilio')(accountSid, authToken);
const service = process.env.Verify_Service
let otpverification = ""
module.exports = createCoreController('api::otp.otp', ({ strapi }) => ({
  async createotp(ctx) {
    console.log("***************");
    console.log("OTP");
    let options = ctx.request.body
    console.log("inside create otp"+options);
    if (options.data.genrate_otp == 'otp') {
      client.verify.v2.services(service)
        .verifications
        .create({ to: '+91' + ctx.request.body.data.Mobile_Number, channel: 'sms' })
    }
    if (options.data.status == 'verifyotp') {
      client.verify.v2.services(service).verificationChecks.create({
        to: '+91' + ctx.request.body.data.Mobile_Number,
        channel: 'sms', code: ctx.request.body.data.Genrate_OTP
      })
        .then(verifications => {
          if (verifications.status == 'approved') {
            otpverification = "approved"
          }
          else if (verifications.status == 'pending') {
            otpverification = "reject"

          }
        }
        )
    }

    if (ctx.request.body.data.Genrate_Message != undefined) {
      client.messages
      .create({
        body: `${ctx.request.body.data.Genrate_Message}`,
        from: process.env.Twilio_phone_number, //the phone number provided by Twillio
        to: '+91' + ctx.request.body.data.Mobile_Number
      })

      //.done());
    }
    if (options.data.email) {
      let Sendgrid;
      if (options.data.status == 'forgotpwd') {
        Sendgrid = await strapi.plugins['email'].services.email.send({
          to: options.data.email,
          from: 'devarshi.vaidya@augustinfotechteam.com',
          replyTo: 'devarshi.vaidya@augustinfotechteam.com',
          subject: "Reset Password for CSM",
          text: `
                  Dear User,
                  Following are the link for reset password :${options.data.urldata}
                  Thank You.       
              `,
          html: `
          <html>
            <body>
            <header>
            <img src="https://my-workspace-image.s3.ap-south-1.amazonaws.com/csm-logo.png"
            height="40px" width="60px" />

            </header>
            <hr>
            <main>
            <table>
                <tr>
                    <td>Dear User,</td>
                    <td></td>
                    <td></td>
                </tr>

                <tr>
                    <td></td>
                    <td></td>
                    <td> Following are the link for reset password :${options.data.urldata}</td>
                  </tr>

                <tr>
                    <td>Thank You.</td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
            </main>
            <hr>
            <footer>
            <p>Copyright © 2022 CSM. All Rights Reserved</p>
            </footer>
            </body>
            </html>`,
        });
      }
      else if (options.data.status == 'changepwd') {
        Sendgrid = await strapi.plugins['email'].services.email.send({
          to: options.data.email,
          from: 'devarshi.vaidya@augustinfotechteam.com',
          replyTo: 'devarshi.vaidya@augustinfotechteam.com',
          subject: "Change Password for CSM",
          text:`
          Dear User,
          Your password changed successfully.
          Thank You
          Copyright © 2022 CSM. All Rights Reserved
          `,
          html: `
              <html>
                <body>
                <header>
                <img src="https://my-workspace-image.s3.ap-south-1.amazonaws.com/csm-logo.png"
                height="40px" width="60px" />

                </header>
                <hr>
                <main>
                <table>
                    <tr>
                        <td>Dear User,</td>
                        <td></td>
                        <td></td>
                    </tr>

                    <tr>
                        <td></td>
                        <td></td>
                        <td>Your password changed successfully.</td>
                     </tr>

                    <tr>
                        <td>Thank You.</td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
                </main>
                <hr>
                <footer>
                <p>Copyright © 2022 CSM. All Rights Reserved</p>
                </footer>
                </body>
                </html>`,

        });
      }
      else if (options.data.status == 'csv') {
        const fs = require('fs');
        function base64_encode(file) {
          let bitmap = fs.readFileSync(file);
          return new Buffer(bitmap).toString('base64');
        }

        fs.writeFile(`report.csv`, options.data.Genrate_csv, (err) => {
          if (err) {
          }
          else {
            let data_base64 = base64_encode(`report.csv`);
            Sendgrid = strapi.plugins['email'].services.email.send({
              to: options.data.email,
              from: 'devarshi.vaidya@augustinfotechteam.com',
              replyTo: 'devarshi.vaidya@augustinfotechteam.com',
              subject: "Stock Report",
              text:`
                Dear User,
                Stock report for ${options.data.name} is attached below.
                Thank You
                Copyright © 2022 CSM. All Rights Reserved
                `,
              html: `
                <html>
                <body>
                <header>
                <img src="https://my-workspace-image.s3.ap-south-1.amazonaws.com/csm-logo.png"
                height="40px" width="60px" />

                </header>
                <hr>
                <main>
                <table>
                    <tr>
                        <td>Dear User,</td>
                        <td></td>
                        <td></td>
                    </tr>

                    <tr>
                        <td></td>
                        <td></td>
                        <td>Stock report for ${options.data.name} is attached below.</td>
                     </tr>

                    <tr>
                        <td>Thank You.</td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
                </main>
                <hr>
                <footer>
                <p>Copyright © 2022 CSM. All Rights Reserved</p>
                </footer>
                </body>
                </html>      
                    `,
              attachments: [
                {
                  filename: `${options.data.name}.csv`,
                  content: data_base64,
                  type: 'application/csv',
                  disposition: 'attachment'
                }
              ]
            });

          }
        })
      } else {
        console.log(options.data.pwd , "data")
        if(options.data.pwd != undefined){
          console.log("inside if");
          Sendgrid = await strapi.plugins['email'].services.email.send({
            to: options.data.email,
            from: 'devarshi.vaidya@augustinfotechteam.com',
            replyTo: 'devarshi.vaidya@augustinfotechteam.com',
            subject: "You have been invited for CSM",
            text: `
                  Dear User,
                    Following are the link for invite user:${`http://cms-ionic-app.s3-website.ap-south-1.amazonaws.com/index`}
                    Username : ${options.data.email}
                    Password : ${options.data.pwd}
                  Thank You.
                  Copyright © 2022 CSM. All Rights Reserved
                `,
            html: `
            <html>
            <body>
            <header>
            <img src="https://my-workspace-image.s3.ap-south-1.amazonaws.com/csm-logo.png"
            height="40px" width="60px" />

            </header>
            <hr>
            <main>
            <table>
                <tr>
                    <td>Dear User,</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>Following are the link for invite user:${`http://cms-ionic-app.s3-website.ap-south-1.amazonaws.com/index`}</td>
                  </tr>
                <tr>
                  <td></td>
                  <td></td>
                  <td>Username : ${options.data.email}</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>Password : ${options.data.pwd}</td>
                </tr>
                <tr>
                    <td>Thank You.</td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
            </main>
            <hr>
            <footer>
            <p>Copyright © 2022 CSM. All Rights Reserved</p>
            </footer>
            </body>
            </html>      
                `,
        });
        }
        else{
          console.log("inside else");
          Sendgrid = await strapi.plugins['email'].services.email.send({
            to: options.data.email,
            from: 'devarshi.vaidya@augustinfotechteam.com',
            replyTo: 'devarshi.vaidya@augustinfotechteam.com',
            subject: "You have been invited for CSM",
            text: `
                    Dear User,
                    Following are the link for invite user:${`http://cms-ionic-app.s3-website.ap-south-1.amazonaws.com/index`}
                    Thank You.
                    Copyright © 2022 CSM. All Rights Reserved
                `,
            html: `
                <html>
                <body>
                <header>
                <img src="https://my-workspace-image.s3.ap-south-1.amazonaws.com/csm-logo.png"
                height="40px" width="60px" />
    
                </header>
                <hr>
                <main>
                <table>
                    <tr>
                        <td>Dear User,</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>Following are the link for invite user:${`http://cms-ionic-app.s3-website.ap-south-1.amazonaws.com/index`}</td>
                    </tr>
                    <tr>
                        <td>Thank You.</td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
                </main>
                <hr>
                <footer>
                <p>Copyright © 2022 CSM. All Rights Reserved</p>
                </footer>
                </body>
                </html>      
                    `,
          });
        }
     
      }

      ctx.send(Sendgrid)
    }


    // some more logic

    const delay = (delayInms) => {
      return new Promise(resolve => setTimeout(resolve, delayInms));
    }



      if (options.data.status == 'verifyotp') {
        let delays = await delay(2000);
        let response = {"status":"","data":{}} ;
        //response.data = { "otp_status": otpverification }
        response.status=200
        response.data={ "otp_status": otpverification }
        return response
  
      }

      else {
          let delays = await delay(2000);
          let response = {}
          return response
  
      }


  }
}));
