'use strict';

/**
 * material-unit router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::material-unit.material-unit');
