'use strict';

/**
 * material-unit service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::material-unit.material-unit');
