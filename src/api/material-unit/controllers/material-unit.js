'use strict';

/**
 *  material-unit controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::material-unit.material-unit');
