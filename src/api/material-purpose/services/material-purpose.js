'use strict';

/**
 * material-purpose service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::material-purpose.material-purpose');
