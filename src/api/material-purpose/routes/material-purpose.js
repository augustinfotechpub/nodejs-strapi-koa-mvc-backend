'use strict';

/**
 * material-purpose router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::material-purpose.material-purpose');
