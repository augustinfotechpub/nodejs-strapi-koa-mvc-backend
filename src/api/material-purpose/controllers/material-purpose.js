'use strict';

/**
 *  material-purpose controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::material-purpose.material-purpose');
