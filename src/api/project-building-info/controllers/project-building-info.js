'use strict';


/**
 *  project-building-info controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::project-building-info.project-building-info', ({ strapi }) =>  ({
    async createmany(ctx) {
        console.log("************************");
        console.log(ctx.request.body.data);
        console.log("************************");
        let response;
	const dateTime = new Date();
        if (Array.isArray(ctx.request.body.data)) {
                  //  if array, loop and return created content.
                  const resArray=[];
                  
                  ctx.request.body.data.forEach(data => {
			  data['publishedAt'] = dateTime
		          console.log("data"+JSON.stringify(data));
	                  response = strapi.db.query('api::project-building-info.project-building-info').create({data,});
                      
                  });
                 
                  //return 'array of content created';
                } else {
                 // return strapi.services.warehouse.add(ctx.request.body);
                }
        return response;
    },
    async deletemany(ctx) {
        console.log("************************");
        console.log(ctx.request.body.data);
        console.log("************************");
        // some logic here
        // const response = await strapi.db.query('api::project-building-info.project-building-info').createMany({
        //     data: ctx.request.body.data,
        // });
        // some more logic
        let response='ok';
        if (Array.isArray(ctx.request.body.data)) {
                  //  if array, loop and return created content.
                  const resArray=[];
                  
                  ctx.request.body.data.forEach(data => {
                      console.log("data"+JSON.stringify(data));
                     response = strapi.db.query('api::project-building-info.project-building-info').delete({
                        where: {'id': data.id}
                      });
                      
                  });
                 
                  //return 'array of content created';
                } else {
                 // return strapi.services.warehouse.add(ctx.request.body);
                }
        return response;
    },
}));

// async ctx => {
//     if (Array.isArray(ctx.request.body)) {
//       //  if array, loop and return created content.
//       return 'array of content created';
//     } else {
//       return strapi.services.warehouse.add(ctx.request.body);
//     }
//   }
