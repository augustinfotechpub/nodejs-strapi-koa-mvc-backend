module.exports = {
    routes: [
      {
        method: 'POST',
        path: '/project-building-infos/create-many',
        handler: 'project-building-info.createmany',
        config: {
            auth: false,
          },
      },
      {
        method: 'POST',
        path: '/project-building-infos/delete-many',
        handler: 'project-building-info.deletemany',
        config: {
            auth: false,
          },
      },
    ],
};