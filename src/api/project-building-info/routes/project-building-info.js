'use strict';

/**
 * project-building-info router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::project-building-info.project-building-info');
