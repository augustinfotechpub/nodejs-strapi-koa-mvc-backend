'use strict';

/**
 * project-building-info service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::project-building-info.project-building-info');
