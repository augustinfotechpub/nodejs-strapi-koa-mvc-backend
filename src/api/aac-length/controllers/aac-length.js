'use strict';

/**
 *  aac-length controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::aac-length.aac-length');
