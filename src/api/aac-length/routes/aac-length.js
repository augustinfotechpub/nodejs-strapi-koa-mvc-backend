'use strict';

/**
 * aac-length router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::aac-length.aac-length');
