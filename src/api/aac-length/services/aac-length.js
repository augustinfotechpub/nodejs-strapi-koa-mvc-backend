'use strict';

/**
 * aac-length service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::aac-length.aac-length');
