'use strict';

/**
 *  state controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::state.state')
, ({ strapi }) => ({
    async find(ctx) {
        const { query } = ctx;
        const entity = await strapi.entityService.findMany('api::state.state', {
            ...query,
            populate: {
                state : {
                    populate:{
                        districts: true
                }
            },
            districts :{
                populate:{
                    state_name:true
                }
            }
            },
        });
        const sanitizedEntity = await this.sanitizeOutput(entity, ctx);
        return this.transformResponse(sanitizedEntity);
    }
})
