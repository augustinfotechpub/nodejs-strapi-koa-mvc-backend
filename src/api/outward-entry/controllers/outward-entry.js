'use strict';

/**
 *  outward-entry controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::outward-entry.outward-entry');
