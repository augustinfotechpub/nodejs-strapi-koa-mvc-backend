'use strict';

/**
 * user-workspace service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::user-workspace.user-workspace');
