'use strict';

/**
 * user-workspace router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::user-workspace.user-workspace');
