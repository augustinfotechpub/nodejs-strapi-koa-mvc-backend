'use strict';

/**
 *  user-workspace controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::user-workspace.user-workspace');
