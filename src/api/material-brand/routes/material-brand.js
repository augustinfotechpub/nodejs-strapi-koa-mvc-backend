'use strict';

/**
 * material-brand router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::material-brand.material-brand');
