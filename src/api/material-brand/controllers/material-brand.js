'use strict';

/**
 *  material-brand controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::material-brand.material-brand');
