'use strict';

/**
 * material-brand service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::material-brand.material-brand');
