'use strict';

/**
 * aac-height service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::aac-height.aac-height');
