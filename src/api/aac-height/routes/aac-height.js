'use strict';

/**
 * aac-height router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::aac-height.aac-height');
