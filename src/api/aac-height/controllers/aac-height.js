'use strict';

/**
 *  aac-height controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::aac-height.aac-height');
