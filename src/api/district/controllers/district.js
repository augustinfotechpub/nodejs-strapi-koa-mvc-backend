'use strict';

/**
 *  district controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

 module.exports = createCoreController('api::district.district');
// ({ strapi }) => ({
//     async find(ctx) {
//         // const entries = await strapi.db.query('api::district.district').findMany({
//         //     select: ['district_name', 'state_name'],
//         //     populate: { state_name: true },
//         // });
           

//         const { query } = ctx;
//         const entity = await strapi.entityService.findMany('api::district.district', {...query,
//             populate: {
//                 cities : {
//                     populate:{`city_name`:true}
//                 }
                
//             }
//         });
//         const sanitizedEntity = await this.sanitizeOutput(entity, ctx);
//         return this.transformResponse(sanitizedEntity);
//     }
// })
