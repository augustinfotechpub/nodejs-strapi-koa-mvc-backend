'use strict';

/**
 * material-truck router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::material-truck.material-truck');
