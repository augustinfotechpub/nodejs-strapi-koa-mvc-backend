'use strict';

/**
 * material-truck service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::material-truck.material-truck');
