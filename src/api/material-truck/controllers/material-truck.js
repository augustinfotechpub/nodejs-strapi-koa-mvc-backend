'use strict';

/**
 *  material-truck controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::material-truck.material-truck');
